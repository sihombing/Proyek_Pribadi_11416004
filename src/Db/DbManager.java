/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.pengumuman;

/**
 *
 * @author Andre Rey
 */
public class DbManager {
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:~/proyekpribadi";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "";
    private Connection connection;
    private Statement stmt;

   
    public DbManager() {
        openConnection();
    }

    public void openConnection() {
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            stmt = connection.createStatement();
        } catch (Exception ex) {
        }
    }
    
    public boolean addPengumuman(pengumuman kantin){
        System.out.println(kantin.toString());
        try{
            String sql = "insert into pengumuman(judul, isi) values ('"
                    + kantin.getJudul() + "',"
                    + "'" + kantin.getIsi() + "')";
            System.out.println(sql);
            stmt.execute(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean updatePengumuman(pengumuman kantin, String keyword){
        System.out.println(kantin.toString());
        try {
            String sql = "UPDATE Pengumuman SET judul = '" + kantin.getJudul()+ "', isi= "
                    + " '" + kantin.getIsi() + "' WHERE id = " + keyword + "";
            System.out.println(sql);
            stmt.execute(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;   
    }
    
    public void hapusPengumuman(String keyword){
        try {
            String sql = "DELETE FROM Pengumuman where id = " + keyword + "";
            stmt.execute(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    public ArrayList<pengumuman> getAllPengumuman(){
        ArrayList<pengumuman> arr  = new ArrayList<>();
        
        try{
            connection = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
            stmt = connection.createStatement();
            
            String sql = "select * from pengumuman";
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()){
                pengumuman kantin = new pengumuman();
                kantin.setId(rs.getInt("id"));
                kantin.setJudul(rs.getNString("judul"));
                kantin.setIsi(rs.getNString("isi"));
                arr.add(kantin);
            }
            return arr;
        } catch (Exception ex){
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arr;
    }
    
    public boolean checkLogin(String username, String pass){
        try {
            String sql = "SELECT * FROM user WHERE username = '" + username + "' AND password = '" + pass + "'";
            System.out.println(sql);
            stmt.execute(sql);
            System.out.println();
            ResultSet rs =  stmt.executeQuery(sql);
            if (rs.first()) {
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("Error In DB");
        }
        return false;
    }
}
