/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kantin;

import Db.DbManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.pengumuman;

/**
 *
 * @author Andre Rey
 */
public class FormPengumumanController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField judul;
    @FXML
    private TextArea isi;
    @FXML
    private Button btnsubmit;
    @FXML
    private Text alert;
    @FXML
    private Button cancel;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void judulaction(ActionEvent event) {
    }

    @FXML
    private void isiaction(MouseEvent event) {
    }

    @FXML
    private void btnsubmitaction(ActionEvent event) throws IOException{
        pengumuman p = new pengumuman();
        
        p.setJudul(judul.getText());
        p.setIsi(isi.getText());
        
        DbManager dm = new DbManager();
        if (dm.addPengumuman(p)){
            alert.setText("Berhasil Menambah Pengumuman");
            System.out.println("berhasil");
            Stage stage = new Stage();
            ((Node) (event.getSource())).getScene().getWindow().hide();
            Parent root = FXMLLoader.load(getClass().getResource("ViewPengumuman.fxml"));

            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } else {
            alert.setText("gagal Menambah Pengumuman");
            System.out.println("gagal");
        }
    }    

    @FXML
    private void btncancelaction(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Parent root = FXMLLoader.load(getClass().getResource("ViewPengumuman.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
}


