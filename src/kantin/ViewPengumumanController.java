/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kantin;

import Db.DbManager;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.pengumuman;

/**
 * FXML Controller class
 *
 * @author Andre Rey
 */
public class ViewPengumumanController implements Initializable {    
    @FXML
    private TableView<pengumuman> tview;
    @FXML
    private TableColumn<?, ?> viewjudul;
    @FXML
    private TableColumn<?, ?> viewisi;
    @FXML
    private TableColumn<?, ?> viewno;
    @FXML
    private Label label;
    @FXML
    private Text alert;
    private Object allProduct;
    public static int id_toUpdate;
    public String test;
    ObservableList<pengumuman> data;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        viewno.setCellValueFactory(new PropertyValueFactory<>("id"));
        viewjudul.setCellValueFactory(new PropertyValueFactory<>("judul"));
        viewisi.setCellValueFactory(new PropertyValueFactory<>("isi"));
        
        data = FXCollections.observableArrayList();
        tview.setItems(data);
        
        showAllProduct();
        getPosition();
    }    
    
    public void showAllProduct(){
        ArrayList<pengumuman> allData = getShowAllBeach();
        
        for(int i=0; i<allData.size(); i++){
            setShowAllBeach(allData, i);
        }     
    }
    
    private ArrayList<pengumuman> getShowAllBeach() {
        DbManager db = new DbManager();
        ArrayList<pengumuman> allProduct = db.getAllPengumuman();
        
        return allProduct;
    }

    private void setShowAllBeach(ArrayList<pengumuman> allData, int i) {
        pengumuman kantin = new pengumuman();
        
        kantin.setId(allData.get(i).getId());
        kantin.setJudul(allData.get(i).getJudul());
        kantin.setIsi(allData.get(i).getIsi());
        data.add(kantin);
    }   
    
    public void getPosition(){
        tview.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList selectedcells = tview.getSelectionModel().getSelectedCells();
        
        selectedcells.addListener(new ListChangeListener() {

            @Override
            public void onChanged(ListChangeListener.Change c) {
                TablePosition tablePosition = (TablePosition) selectedcells.get(0);
                int position = tablePosition.getRow();
                id_toUpdate = data.get(position).getId();
                System.out.println(id_toUpdate);
                test  = data.get(position).getJudul();
                System.out.println(test);
            }
        });        
    }      

    @FXML
    private void gocreate(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Parent root = FXMLLoader.load(getClass().getResource("FormPengumuman.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void goupdate(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Parent root = FXMLLoader.load(getClass().getResource("UpdatePengumuman.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void godelete(ActionEvent event) throws IOException {
        DbManager db = new DbManager();
        String keyword = Integer.toString(id_toUpdate);
        db.hapusPengumuman(keyword);
        
        System.out.println("berhasil");
        data.clear();
        showAllProduct();
        Stage stage = new Stage();
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Parent root = FXMLLoader.load(getClass().getResource("ViewPengumuman.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
    
}