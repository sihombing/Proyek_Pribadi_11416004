/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kantin;

import Db.DbManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.pengumuman;

/**
 * FXML Controller class
 *
 * @author Andre Rey
 */
public class UpdatePengumumanController implements Initializable {
    @FXML
    private Label label;
    @FXML
    private TextField judul;
    @FXML
    private TextArea isi;
    @FXML
    private Button btnupdate;
    @FXML
    private Text alert;
    public ViewPengumumanController view;
    @FXML
    private Button cancel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        view = new ViewPengumumanController();
    }    


    @FXML
    private void btnupdateaction(ActionEvent event) throws IOException {
        pengumuman kantin = new pengumuman();
        
        kantin.setJudul(judul.getText());
        kantin.setIsi(isi.getText());
        
        String keyword = Integer.toString(view.id_toUpdate);
        System.out.println(keyword);
        DbManager dm = new DbManager();
        if (dm.updatePengumuman(kantin, keyword)){
            alert.setText("Berhasil Update Pengumuman");
            System.out.println("berhasil");
            Stage stage = new Stage();
            ((Node) (event.getSource())).getScene().getWindow().hide();
            Parent root = FXMLLoader.load(getClass().getResource("ViewPengumuman.fxml"));

            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } else {
            alert.setText("gagal update  Pengumuman");
            System.out.println("gagal");
        }
    }

    @FXML
    private void btncancelaction(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Parent root = FXMLLoader.load(getClass().getResource("ViewPengumuman.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
    
}
