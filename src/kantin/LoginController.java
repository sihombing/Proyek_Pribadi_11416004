/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kantin;

import Db.DbManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Andre Rey
 */
public class LoginController implements Initializable {
    @FXML
    private Label label;
    @FXML
    private Text alert;
    @FXML
    private Button login;
    private Object password;
    @FXML
    private TextField Username;
    @FXML
    private TextField Password;
    @FXML
    private Text status;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btnlogin(ActionEvent event) throws IOException {
        DbManager db = new DbManager();
        if (db.checkLogin(Username.getText(), Password.getText())) {
            Stage stage = new Stage();
            ((Node) (event.getSource())).getScene().getWindow().hide();
            Parent root = FXMLLoader.load(getClass().getResource("ViewPengumuman.fxml"));

            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.show();
        } else {
            status.setText("Username or Password Salah");
        }

    }
    
}
